import React, {Component} from  'react';
import {
    View,
    Text,
    Image,
    TextInput,
    ScrollView,
    TouchableHighlight,
    TouchableOpacity, Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import Button from 'apsl-react-native-button';
import styles from '../styles';

const email = require('../../assets/email-white.png');
const passwd = require('../../assets/password-white.png');
const arrowdown = require('../../assets/arrow_down.png');

class Index extends Component{

    constructor(props){
        super(props);
        this.state = {
            type: 'Patient',
            fullname: "",
            email: "",
            password: "",
            birthday: "",
            address: "",
            phonenumber: '',
            emergencynumber: '',

            day: "Day",
            month: "Month",
            year: "Year",

            step:0,
            showModal2: null,
            showModal: false,
        };

        this.renderStep = this.renderStep.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.setDateModal = this.setDateModal.bind(this);
        this.renderDateItem = this.renderDateItem.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    renderDateItem(){
        if (this.state.showModal2==='day'){
            let data = [];
            for (let i=1;i<=31;i++) {
                data.push(
                    <TouchableHighlight key={i}
                        onPress={() => {
                            this.setState({day:i});
                            this.setDateModal(null);
                        }}>
                        <Text style={this.state.day===i ? styles.modalItemSelected : styles.modalItem}>{i}</Text>
                    </TouchableHighlight>
                )
            }
            return data
        } else if (this.state.showModal2==='month'){
            let data = [];
            for (let i=1;i<=12;i++) {
                data.push(
                    <TouchableHighlight key={i}
                        onPress={() => {
                            this.setState({month:i});
                            this.setDateModal(null);
                        }}>
                        <Text style={this.state.month===i ? styles.modalItemSelected : styles.modalItem}>{i}</Text>
                    </TouchableHighlight>
                )
            }
            return data
        }else if (this.state.showModal2==='year'){
            let data = [];
            for (let i=(new Date().getFullYear()-100);i<=new Date().getFullYear();i++) {
                data.push(
                    <TouchableOpacity key={i}
                                        onPress={() => {
                                            this.setState({year:i});
                                            this.setDateModal(null);
                                        }}>
                        <Text style={this.state.year===i ? styles.modalItemSelected : styles.modalItem}>{i}</Text>
                    </TouchableOpacity>
                )
            }
            return data
        }
    };
    setModalVisible(visible) {
        this.setState({showModal: visible});
    };
    setDateModal = (name) =>{
        this.setState({showModal2: name});
    };
    renderStep = () => {
        switch (this.state.step){
            case 0: return (
                <View style={{width:"100%"}}>
                    <TouchableOpacity onPress={() => {this.setModalVisible(true)}}
                                      style={styles.dropdown}>
                        <Text style={{color: "#70CB94",fontSize: Platform.OS === 'ios' ? 18 : 14,}}>{this.state.type}</Text>
                        <Image source={arrowdown}
                               style={{width:15,resizeMode: 'contain',marginRight:12}}/>
                    </TouchableOpacity>

                    <View style={styles.inputWrapper}>
                        <Image source={email}
                               style={styles.iconStyle}/>

                        <TextInput
                            underlineColorAndroid="transparent"
                            style={styles.textInputWrapper}
                            onChangeText={(text) => (this.onChange('email',text))}
                            placeholder="Email"
                            placeholderTextColor="white"
                            value={this.state.email}
                            returnKeyType="next"
                        />
                    </View>

                    <View style={styles.inputWrapper}>
                        <Image source={passwd}
                               style={styles.iconStyle}/>

                        <TextInput
                            underlineColorAndroid="transparent"
                            style={styles.textInputWrapper}
                            onChangeText={(text) => (this.onChange('password',text))}
                            placeholder="Password"
                            secureTextEntry={true}
                            placeholderTextColor="white"
                            value={this.state.password}
                            returnKeyType="go"
                        />
                    </View>

                </View>
            );
            case 1: return(
                <View style={{width:"100%"}}>
                    <TextInput
                        underlineColorAndroid="transparent"
                        autoCapitalize={'words'}
                        style={styles.textInput}
                        onChangeText={(text) => (this.onChange('fullname',text))}
                        placeholder="Full Name"
                        placeholderTextColor="white"
                        value={this.state.fullname}
                        returnKeyType="next"
                    />
                    <TextInput
                        underlineColorAndroid="transparent"
                        style={styles.textInput}
                        onChangeText={(text) => (this.onChange('phonenumber',text))}
                        placeholder="Phone Number"
                        placeholderTextColor="white"
                        secureTextEntry={false}
                        value={this.state.phonenumber}
                        returnKeyType="next"
                        keyboardType='phone-pad'
                    />
                    {this.state.type === 'Patient' ? (<TextInput
                        underlineColorAndroid="transparent"
                        style={styles.textInput}
                        onChangeText={(text) => (this.onChange('emergencynumber',text))}
                        placeholder="Emergency Phone Number"
                        placeholderTextColor="white"
                        value={this.state.emergencynumber}
                        returnKeyType="next"
                        keyboardType='phone-pad'
                    />): (null)}

                    <TextInput
                        underlineColorAndroid="transparent"
                        style={styles.textInputMultiline}
                        onChangeText={(text) => (this.onChange('address',text))}
                        placeholder="Address"
                        multiline={true}
                        numberOfLines = {4}
                        placeholderTextColor="white"
                        value={this.state.address}
                        returnKeyType="next"
                    />
                    <View style={{flexDirection:"row"}}>
                        <TouchableOpacity onPress={() => {this.setDateModal("day")}}
                                          style={styles.dropdownDate}>
                            <Text style={{color: "white",fontSize: Platform.OS === 'ios' ? 18 : 14,}}
                            >{this.state.day}</Text>
                            <Image source={arrowdown}
                                   style={{width:15,resizeMode: 'contain',marginRight:12}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.setDateModal("month")}}
                                          style={styles.dropdownDate}>
                            <Text style={{color: "white",fontSize: Platform.OS === 'ios' ? 18 : 14,}}
                            >{this.state.month}</Text>
                            <Image source={arrowdown}
                                   style={{width:15,resizeMode: 'contain',marginRight:12}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.setDateModal("year")}}
                                          style={styles.dropdownDate}>
                            <Text style={{color: "white",fontSize: Platform.OS === 'ios' ? 18 : 14,}}
                            >{this.state.year}</Text>
                            <Image source={arrowdown}
                                   style={{width:15,resizeMode: 'contain',marginRight:12}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

    };
    onChange(name,text){
        this.setState({[name]:text})
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={{marginTop:"15%"}}></View>
                {this.renderStep()}

                <View style={{marginTop:30,width:"100%"}}>
                    {this.state.step ===0 ?
                        <Button style={styles.button}
                                textStyle={{color:"#fff"}}
                                onPress = {() => (this.setState({step: this.state.step+1}))}>Next</Button>
                    : <Button style={styles.button}
                              textStyle={{color:"#fff"}}
                              onPress = {() => this.props.navigation.navigate('Login')}>SIGN UP</Button>}
                </View>
                <View style={{flexDirection:"row",marginTop:'3%',height:"100%"}}>
                    <Text style={{fontSize:14,color:"white"}}>Already have an account?  </Text>
                    <Text onPress={() => this.props.navigation.navigate('Login')} style={{fontSize:14,color:"#DEE3ED"}}>Sign in</Text>
                </View>

                <Modal isVisible={this.state.showModal}
                       onBackdropPress={() => this.setState({isClose: !this.state.isClose})}>
                    <View style={styles.modalContent}>
                        <ScrollView>
                            <TouchableHighlight
                                style={{width:"100%"}}
                                onPress={() => {
                                    this.setState({type:'Patient'});
                                    this.setModalVisible(!this.state.showModal);
                                }}>
                                <Text style={this.state.type==="Patient" ? styles.modalItemSelected : styles.modalItem}>Patient</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={{width:"100%"}}
                                onPress={() => {
                                    this.setState({type:'Doctor'});
                                    this.setModalVisible(!this.state.showModal);
                                }}>
                                <Text style={this.state.type==="Doctor" ? styles.modalItemSelected : styles.modalItem}>Doctor</Text>
                            </TouchableHighlight>
                        </ScrollView>
                    </View>
                </Modal>
                <Modal isVisible={this.state.showModal2 !==null}
                       onBackdropPress={() => this.setState({isClose: !this.state.isClose})}>
                    <View style={styles.modalContent}>
                        <ScrollView style={{width:"100%"}}>
                            <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                                {this.renderDateItem()}
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default Index;


