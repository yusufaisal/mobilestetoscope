import React,{Component} from 'react';
import {
    View,
    Text,
    ScrollView, Platform,
    Image
} from 'react-native';

import styles from '../styles';

const Photo = require('../../assets/skeleton.jpg');

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            photo : undefined,
        }
    }

    render(){
        return(
            <View style={{height: "100%"}}>
                <ScrollView>
                    <View style={{width:"100%",height:200,backgroundColor:"white"}}>
                        <Image source={this.state.photo===undefined ? Photo : this.state.photo}
                               style={{width:"100%",height:"100%"}} resizemode='cover'/>
                    </View>
                    <View style={styles.containerGray}>
                        <View style={{width:"100%",flex:1,padding:10, marginTop:10,justifyContent:"flex-start"}}>
                            <Text style={{fontSize:Platform.OS === 'ios' ? 16 : 12}}>Profile Information</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>User ID</Text>
                                <Text style={styles.cardItemLabel}>9001 0021</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Full Name</Text>
                                <Text style={styles.cardItemLabel}>Yusuf Faisal</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Phone Number</Text>
                                <Text style={styles.cardItemLabel}>62812333000</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Emergency Number</Text>
                                <Text style={styles.cardItemLabel}>05114770000</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Email</Text>
                                <Text style={styles.cardItemLabel}>yusufaisal9@gmail.com</Text>
                            </View>
                        </View>

                        <View style={{width:"100%",flex:1,padding:10, marginTop:10,justifyContent:"flex-start"}}>
                            <Text style={{fontSize:Platform.OS === 'ios' ? 16 : 12}}>Personal Details</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Birthday</Text>
                                <Text style={styles.cardItemLabel}>1 May 1997</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Age</Text>
                                <Text style={styles.cardItemLabel}>21</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Gender</Text>
                                <Text style={styles.cardItemLabel}>Male</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Address</Text>
                                <Text style={styles.cardItemLabel}> Jl. Telekomunikasi 1</Text>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Index;