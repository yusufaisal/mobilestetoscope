import React,{Component} from 'react';
import {View,
        Text,
        Image,
        ScrollView,
        TouchableOpacity,TouchableHighlight
} from 'react-native';
import { VictoryLine,VictoryClipContainer,VictoryScatter, VictoryChart, VictoryTheme } from "victory-native";
import Modal from 'react-native-modal';
import styles from '../styles';

const arrowdown = require('../../assets/arrow-down.png');
const arrowright = require('../../assets/arrow-right.png');
const info = require('../../assets/info.png');

class Dashboard extends Component{
    constructor(props){
        super(props);
        this.state = {
            isClose: true,
            data: this.props.navigation.getParam('data')
        }
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{width:"100%",backgroundColor:"white",justifyContent:"center",alignItems:"center"}}>
                    <VictoryChart
                        theme={VictoryTheme.material}>
                        <VictoryLine
                            style={{
                                data: { stroke: "#74ff7c" },
                                parent: {
                                    border: "1px solid #ccc"
                                }
                            }}
                            data={[
                                { x: 1, y: 2 },
                                { x: 2, y: 4 },
                                { x: 3, y: 1 },
                                { x: 4, y: 5 }
                            ]}
                        />
                    </VictoryChart>
                </View>
                <View style={styles.devider}></View>
                <View style={styles.containerGray}>
                    <View style={styles.card}>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Condition</Text>
                            <Text style={styles.cardItemLabel}>{this.state.data.condition}</Text>
                        </View>
                        <View style={styles.devider}></View>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Heart Beat</Text>
                            <Text style={styles.cardItemLabel}>{this.state.data.beat} bpm</Text>
                        </View>
                        <View style={styles.devider}></View>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Timestamp</Text>
                            <Text style={styles.cardItemLabel}>Today, {this.state.data.hour}:{this.state.data.minute}</Text>
                        </View>
                    </View>
                </View>

                <Modal isVisible={!this.state.isClose}
                       onBackdropPress={() => this.setState({isClose: !this.state.isClose})}>
                    <View style={styles.modalContent}>
                        <ScrollView style={{width:"100%"}}>
                            <View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Full Name</Text>
                                    <Text style={styles.cardItemLabel}>Yusuf Faisal</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Email</Text>
                                    <Text style={styles.cardItemLabel}>yusufaisal9@gmail.com</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Birthday</Text>
                                    <Text style={styles.cardItemLabel}>1 May 1997</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Age</Text>
                                    <Text style={styles.cardItemLabel}>21</Text>
                                </View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Gender</Text>
                                    <Text style={styles.cardItemLabel}>Male</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default Dashboard;