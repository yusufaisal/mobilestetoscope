import {Platform,StyleSheet} from "react-native";

// const color = {
//     root:{
//         blue: "#53B9EC",
//         grey: "#DEE3ED",
//         darkgrey: "#acb1bb",
//         white: "#FFFFFF",
//         green: "#70CB94",
//         red: "#f55151",
//     },
// };

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"#53B9EC",
        alignItems: 'center',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:20,
        justifyContent: "space-between"
    },
    containerGray: {
        flex: 1,
        backgroundColor:"#DEE3ED",
        alignItems: 'center',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom:20,
    },
    containerWhite: {
        flex: 1,
        backgroundColor:"white",
        alignItems: 'center',
        paddingLeft:20,
        paddingRight:20,
    },
    button: {
        backgroundColor:'#70CB94',
        borderColor:'#70CB94',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.18,
        shadowRadius: 2.5,
        elevation:3,
    },
    inputWrapper:{
        width:"100%",
        flexDirection:"row",
        borderColor: 'white',
        borderBottomWidth: 1,
        marginTop:7,
        marginBottom:7,
    },
    textInputWrapper: {
        marginTop:6,
        color: "white",
        fontSize: Platform.OS === 'ios' ? 18 : 14,
        height: 40,
        width:'100%',
    },
    dropdown: {
        flexDirection:"row",
        justifyContent:"space-between",
        paddingLeft:10,
        paddingTop:9,
        backgroundColor:"white",
        marginTop:7,
        marginBottom:7,
        height: 40,
        width:'100%',
        borderRadius: 4,
    },
    dropdownDate: {
        flex:3,
        flexDirection:"row",
        justifyContent:"space-between",
        paddingLeft:10,
        paddingTop:9,

        borderColor:"white",
        borderBottomWidth:1,
        borderRadius: 4,

        marginTop:7,
        marginLeft:2,
        marginRight:2,
        marginBottom:7,
        height: 40,
        width:'100%',
    },
    textInput: {
        marginTop:7,
        marginBottom:7,
        color: "white",
        fontSize: Platform.OS === 'ios' ? 18 : 14,
        height: 40,
        width:'100%',
        borderColor: 'white',
        borderBottomWidth: 1,
    },
    textInputMultiline: {
        marginTop:7,
        marginBottom:7,
        color: "white",
        fontSize: Platform.OS === 'ios' ? 18 : 14,
        height: 80,
        borderColor: 'white',
        borderBottomWidth: 1,
        width:'100%'
    },
    iconStyle:{
        width: 30,
        resizeMode: 'contain',
        borderColor: 'white',
        borderBottomWidth: 1,
        marginRight:9
    },
    modalContent:{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 22,
        borderRadius: 4,
        maxHeight:"50%",
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalItem:{
        alignItems:"center",
        justifyContent:"center",
        paddingTop:7,
        width:"100%",
        fontSize:20,
        paddingBottom:7,
        color: "#acb1bb",
    },
    modalItemSelected:{
        paddingTop:7,
        width:"100%",
        fontSize:20,
        paddingBottom:7,
        color: "#000000",
    },
    logo: {
        marginTop:"10%",
        marginBottom:"5%",
        backgroundColor:"#DEE3ED",
        width:222,
        height:154,
    },
    card:{
        marginTop:10,
        width:"100%",
        backgroundColor:"white",
        borderColor:"#acb1bb",
        borderRadius:2,
        borderWidth:1,

        // // iOS Shadow
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.18,
        // shadowRadius: 2.5,
        //
        // // Android Shadow
        // elevation: 3
    },
    cardItem:{
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center",

    },
    cardItemHeader:{
        paddingLeft:10,
        paddingRight:10,
        paddingTop:13,
        paddingBottom:13,
        fontSize:Platform.OS === 'ios' ? 18 : 14,
    },
    cardItemLabel:{
        paddingLeft:10,
        paddingRight:10,
        paddingTop:13,
        paddingBottom:13,
        color:"#acb1bb",
        fontSize:Platform.OS === 'ios' ? 18 : 14,
    },
    devider:{
        borderColor:"#DEE3ED",
        borderWidth:0.5,
    }
});