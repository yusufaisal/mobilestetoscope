import React,{Component} from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity, TouchableHighlight, Platform
} from 'react-native';
import Modal from 'react-native-modal';
import styles from '../styles';

const arrowdown = require('../../assets/arrow-down.png');
const arrowright = require('../../assets/arrow-right.png');
const info = require('../../assets/info.png');

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            isClose: true
        };

        this.renderSample = this.renderSample.bind(this);
        this.getRand = this.getRand.bind(this);
    }

    getRand(min,max){
        return Math.floor(min + Math.random() *(max-min));
    }

    renderSample(){
        v = [];
        let hour = 0;
        let beat = 0;
        let minute = 0;
        let data = [];
        for (let i=0; i<15; i++) {
            hour = this.getRand(0,23);
            minute = this.getRand(0,59);
            beat = this.getRand(60,100);
            data.push({
                hour: hour,
                minute: minute,
                beat: beat,
                condition: "Normal"
            });

            v.push(<TouchableOpacity key={i} style={styles.cardItem}
                                onPress={() => (this.props.navigation.navigate("Detail",{data:data[i]}))}>
                <View>
                    <Text style={styles.cardItemHeader}>Normal</Text>
                    <Text style={{
                        color:"#acb1bb",
                        paddingLeft:10,
                        paddingRight:10,
                        paddingBottom:13,
                        fontSize:Platform.OS === 'ios' ? 14 : 10,
                    }}>{beat} bpm</Text>
                </View>

                <View style={{flexDirection:"row"}}>
                    <Text style={styles.cardItemLabel}>Today, {hour}:{minute}</Text>
                    <Image source={arrowright}
                           style={{width:15,resizeMode: 'contain',marginTop:6,marginRight:12}}/>
                </View>
            </TouchableOpacity>)

            v.push(<View key={i+15} style={styles.devider}></View>)
        }
        return v
    }

    render(){
        return(
            <View style={{backgroundColor:"#DEE3ED"}}>
                <ScrollView style={{width:"100%"}}>
                    <View style={styles.containerGray}>
                        <View style={styles.card}>
                            {this.renderSample()}
                        </View>
                    </View>
                </ScrollView>
            </View>
    )
    }
}

export default Index;