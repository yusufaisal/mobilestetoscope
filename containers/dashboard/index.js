import React,{Component} from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity, TouchableHighlight, Platform
} from 'react-native';
import Modal from 'react-native-modal';
import { VictoryLine,VictoryClipContainer,VictoryScatter, VictoryChart, VictoryTheme } from "victory-native";
import styles from '../styles';

const arrowdown = require('../../assets/arrow-down.png');
const arrowright = require('../../assets/arrow-right.png');
const info = require('../../assets/info.png');

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            isClose: true
        }
    }

    render(){
        return(
            <View style={{backgroundColor:"#DEE3ED"}}>
                <ScrollView style={{width:"100%"}}>
                    <View style={styles.containerGray}>
                        <View style={{width:"100%",padding:10, marginTop:10,justifyContent:"flex-start"}}>
                            <Text style={{fontSize:Platform.OS === 'ios' ? 16 : 12}}>Personal Information</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Full Name</Text>
                                <View style={{flexDirection:"row"}}>
                                    <Text style={styles.cardItemLabel}>Yusuf Faisal</Text>
                                    <TouchableOpacity
                                        onPress={() => (this.props.navigation.navigate('Info'))}>
                                        <Image source={info}
                                               style={{width:15,resizeMode: 'contain',marginRight:12,marginTop:4}}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>User ID</Text>
                                <Text style={styles.cardItemLabel}>90010021</Text>
                            </View>
                        </View>
                        <View style={{width:"100%",padding:10, marginTop:10,justifyContent:"flex-start"}}>
                            <Text style={{fontSize:Platform.OS === 'ios' ? 16 : 12}}>Device</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Status</Text>
                                <Text style={styles.cardItemLabel}>Not Connected</Text>
                            </View>
                        </View>

                        <View style={{width:"100%",padding:10, marginTop:10,justifyContent:"flex-start"}}>
                            <Text style={{fontSize:Platform.OS === 'ios' ? 16 : 12}}>Last Condition</Text>
                        </View>
                        <View style={styles.card}>
                            <View style={{width:"100%",backgroundColor:"white"}}>
                                <VictoryChart
                                    theme={VictoryTheme.material}>
                                    <VictoryLine
                                        style={{
                                            data: { stroke: "#74ff7c" },
                                            parent: {
                                                border: "1px solid #ccc"
                                            }
                                        }}
                                        data={[
                                            { x: 1, y: 2 },
                                            { x: 2, y: 4 },
                                            { x: 3, y: 1 },
                                            { x: 4, y: 5 }
                                        ]}
                                    />
                                </VictoryChart>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Condition</Text>
                                <Text style={styles.cardItemLabel}>Normal</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>Heart Beat</Text>
                                <Text style={styles.cardItemLabel}>80 bpm</Text>
                            </View>
                            <View style={styles.devider}></View>
                            <View style={styles.cardItem}>
                                <Text style={styles.cardItemHeader}>TimeStamp</Text>
                                <Text style={styles.cardItemLabel}>Today, 9:00</Text>
                            </View>
                        </View>
                        <View style={styles.card}>
                            <TouchableOpacity style={styles.cardItem}
                                              onPress={()=>(this.props.navigation.navigate('History'))}>
                                <Text style={styles.cardItemHeader}>History</Text>
                                <Image source={arrowright}
                                       style={{width:15,resizeMode: 'contain',marginRight:12}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>

        )
    }
}

export default Index;