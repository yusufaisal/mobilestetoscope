import React,{Component} from 'react';
import {View,
        Text,
        Image,
        ScrollView,
        TouchableOpacity,TouchableHighlight
} from 'react-native';
import Modal from 'react-native-modal';
import styles from '../styles';

const arrowdown = require('../../assets/arrow-down.png');
const arrowright = require('../../assets/arrow-right.png');
const info = require('../../assets/info.png');

class Dashboard extends Component{
    constructor(props){
        super(props);
        this.state = {
            isClose: true
        }
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{width:"100%",height:"40%",backgroundColor:"#70CB94"}}></View>
                <View style={styles.containerWhite}>
                    <View style={styles.card}>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Full Name</Text>
                            <View style={{flexDirection:"row"}}>
                                <Text style={styles.cardItemLabel}>Yusuf Faisal</Text>
                                <TouchableOpacity
                                    onPress={() => (this.setState({isClose: !this.state.isClose}))}>
                                    <Image source={info}
                                           style={{width:15,resizeMode: 'contain',marginRight:12,marginTop:4}}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.devider}></View>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Heart Beat</Text>
                            <Text style={styles.cardItemLabel}>80 BPM</Text>
                        </View>
                        <View style={styles.cardItem}>
                            <Text style={styles.cardItemHeader}>Condition</Text>
                            <Text style={styles.cardItemLabel}>Normal</Text>
                        </View>
                    </View>
                </View>

                <Modal isVisible={!this.state.isClose}
                       onBackdropPress={() => this.setState({isClose: !this.state.isClose})}>
                    <View style={styles.modalContent}>
                        <ScrollView style={{width:"100%"}}>
                            <View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Full Name</Text>
                                    <Text style={styles.cardItemLabel}>Yusuf Faisal</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Email</Text>
                                    <Text style={styles.cardItemLabel}>yusufaisal9@gmail.com</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Birthday</Text>
                                    <Text style={styles.cardItemLabel}>1 May 1997</Text>
                                </View>
                                <View style={styles.devider}></View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Age</Text>
                                    <Text style={styles.cardItemLabel}>21</Text>
                                </View>
                                <View style={styles.cardItem}>
                                    <Text style={styles.cardItemHeader}>Gender</Text>
                                    <Text style={styles.cardItemLabel}>Male</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default Dashboard;