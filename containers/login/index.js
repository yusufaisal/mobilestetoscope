import React, {Component} from  'react';
import {
    View,
    Text,
    TextInput,
    Picker,
    KeyboardAvoidingView,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView,
    Image,
    Platform,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import Modal from 'react-native-modal';
import Button from 'apsl-react-native-button';
import styles from '../styles';

const email = require('../../assets/email-white.png');
const passwd = require('../../assets/password-white.png');
const arrowdown = require('../../assets/triangle-down.png');

const resetAction = StackActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Home' }),
    ],
});

class Index extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            item: '',
            type:'Patient',

            isLoading: false,
            showModal: false,
        };

        this.setModalVisible = this.setModalVisible.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    setModalVisible(visible) {
        this.setState({showModal: visible});
    }
    onChange(name,text){
        this.setState({[name]:text})
    }

    componentDidMount(){

    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logo}></View>

                <TouchableOpacity onPress={() => {this.setModalVisible(true)}}
                                  style={styles.dropdown}>
                    <Text style={{color: "#70CB94",fontSize: Platform.OS === 'ios' ? 18 : 14,}}
                    >{this.state.type}</Text>

                </TouchableOpacity>

                <View style={styles.inputWrapper}>
                    <Image source={email}
                           style={styles.iconStyle}/>

                    <TextInput
                        underlineColorAndroid="transparent"
                        style={styles.textInputWrapper}
                        onChangeText={(text) => (this.onChange('email',text))}
                        placeholder="Email"
                        placeholderTextColor="white"
                        value={this.state.email}
                        returnKeyType="next"
                    />
                </View>

                <View style={styles.inputWrapper}>
                    <Image source={passwd}
                           style={styles.iconStyle}/>

                    <TextInput
                        underlineColorAndroid="transparent"
                        style={styles.textInputWrapper}
                        onChangeText={(text) => (this.onChange('password',text))}
                        placeholder="Password"
                        secureTextEntry={true}
                        placeholderTextColor="white"
                        value={this.state.password}
                        returnKeyType="go"
                    />
                </View>


                <View style={{marginTop:30,width:"100%"}}>
                    <Button style={styles.button}
                            textStyle={{color:"#fff"}}
                            onPress = {() => (this.props.navigation.dispatch(resetAction))}
                            isDisabled={this.state.isLoading}>SIGN IN</Button>
                </View>
                <View style={{flexDirection:"row",marginTop:'3%',height:"100%"}}>
                    <Text style={{fontSize:14,color:"white"}}>Doesn't have an account?  </Text>
                    <Text onPress = {() => (this.props.navigation.navigate('SignUp'))}
                          style={{fontSize:14,color:"#DEE3ED"}}>Sign up</Text>
                    {/*onPress={this.state.type==="Patient" ?  () => this.props.navigation.resetTo('PatientDashboard'):() => this.props.navigation.resetTo('DoctorDashboard')} */}
                </View>

                <Modal isVisible={this.state.showModal}
                       onBackdropPress={() => this.setState({isClose: !this.state.isClose})}>
                    <View style={styles.modalContent}>
                        <ScrollView>
                            <TouchableHighlight
                                style={{width:"100%"}}
                                onPress={() => {
                                    this.setState({type:'Patient'});
                                    this.setModalVisible(!this.state.showModal);
                                }}>
                                <Text style={this.state.type==="Patient" ? styles.modalItemSelected : styles.modalItem}>Patient</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={{width:"100%"}}
                                onPress={() => {
                                    this.setState({type:'Doctor'});
                                    this.setModalVisible(!this.state.showModal);
                                }}>
                                <Text style={this.state.type==="Doctor" ? styles.modalItemSelected : styles.modalItem}>Doctor</Text>
                            </TouchableHighlight>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default Index;