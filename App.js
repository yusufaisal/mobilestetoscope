import React from 'react';
import {Button,Image,TouchableOpacity} from 'react-native';
import { createStackNavigator,createTabNavigator,createDrawerNavigator } from 'react-navigation'; // 2.14.2

import LoginScreen from './containers/login/index';
import SignupScreen from './containers/signup/index';
import DashScreen from './containers/dashboard/index';
import UserInfoScreen from './containers/info/index';
import HistoryScreen from './containers/history';
import DetailScreen from './containers/detail';

const burger  = require('./assets/Hamburger_icon.png');
const HomeStack = createStackNavigator({
    Dashboard: {
        screen:DashScreen,
        navigationOptions:{
            title: 'Dashboard',
            headerLeft: (
                <TouchableOpacity>
                    <Image source={burger} style={{width:25, resizeMode: 'contain', marginLeft:18}}/>
                </TouchableOpacity>
            ),

        }
    },
    Info: {
        screen:UserInfoScreen,
        navigationOptions:{
            title: 'Info',
        }
    },
    History: {
        screen:HistoryScreen,
        navigationOptions:{
            title: 'History',
        }
    },
    Detail: {
        screen:DetailScreen,
        navigationOptions:{
            title: 'Detail',
        }
    },
}, {
    /* The header config from HomeScreen is now here */
    navigationOptions: {
        headerStyle: {
            backgroundColor: '#53B9EC',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    },
});

const SignInUp = createStackNavigator({
    Login: {
        screen:LoginScreen,
        navigationOptions:{
            title: 'Sign In',
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    },
    SignUp: {
        screen:SignupScreen,
        navigationOptions:{
            title: 'Sign Up',
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    },
        Home:{ screen: HomeStack},},
    {
        headerMode: 'none'
    }
);

export default class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isAuthenticated : false,
        };
        this.isAuthenticate = this.isAuthenticate.bind(this)

    }


    isAuthenticate(){
        this.setState({isAuthenticated :true})
    }

    render() {
            if(!this.state.isAuthenticated){
                return (<SignInUp Auth={this.isAuthenticate}/>)
            } else {
                return (<HomeStack/>)
            }
    }
}
